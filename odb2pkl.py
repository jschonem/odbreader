#    ******************************************************
#    *****            COPYRIGHT  (C)  2018            *****
#    *****           BY ATA ENGINEERING, INC.         *****
#    *****             ALL RIGHTS RESERVED            *****
#    ******************************************************
#
#    Redistribution and use in source and binary forms, with or without modification, are permitted
#    provided that the following conditions are met:
#
#    1. Redistributions of source code must retain the above copyright notice, this list of
#    conditions and the following disclaimer.
#
#    2. Redistributions in binary form must reproduce the above copyright notice, this list of
#    conditions and the following disclaimer in the documentation and/or other materials
#    provided with the distribution.
#
#    3. Neither the name of the copyright holder nor the names of its contributors may be used
#    to endorse or promote products derived from this software without specific prior written
#    permission.
#
#    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
#    OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
#    THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
#    EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
#    GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
#    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
#    NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
#    OF THE POSSIBILITY OF SUCH DAMAGE.
#
# Joe Schoneman
# joe.schoneman@ata-e.com
# June 2018

"""

This program reads an Abaqus ODB file and writes it out to Python's Pickle format. It must be
called through the Abaqus kernel, and as such only has access to the Python modules which
come with your Abaqus installation (or which you have installed yourself in the Abaqus Python
directory).

USAGE:

abaqus python odb2pkl.py odbfilename.odb - fieldOutputs <Required list of field outputs
    which will be extracted> [-step <Optional Step name>] [-frame <optional frame number>]
    [-save <Optional filename for pickle>]

"""

import pickle
from abaqusReader import OdbReader
import logging
import traceback
import argparse
import numpy as np
import os

parser = argparse.ArgumentParser(description = 'Retrieve data from an Abaqus ODB')
parser.add_argument('odbname', help = 'Required name of ODB')
parser.add_argument('-step', default = None, help = 'Optional step name')
parser.add_argument('-frame', type = int, default = None, help = 'Optional frame number')
parser.add_argument('-fieldOutputs', nargs = '+', help = 'List of field outputs requested',
                    required = True)
parser.add_argument('-save', default = None, type = str,
                    help = 'Optional filename to save to. Default is <odb>_<step>.pkl')

args = parser.parse_args()

# Wrap in try/catch for graceful exit on error
try:

    reader = OdbReader(args.odbname, verbose = True)
    reader.readStep(args.fieldOutputs, stepKey = args.step, frameNumber = args.frame)
    stepname = reader.fieldSteps.keys()[0]

    outputData = reader.fieldSteps[stepname].nodeData
    outputData.extend(reader.fieldSteps[stepname].elementData)

    nFrames = len(reader.fieldSteps[stepname].frames)
    outputs = dict()

    # Obtain the result node ordering and/or element ordering.
    if len(reader.fieldSteps[stepname].frames[0].nodeOutputs) > 0:
        nodeOutput = reader.fieldSteps[stepname].nodeData[0]
        nodeKeys = reader.fieldSteps[stepname].frames[0].nodeOutputs[nodeOutput].dataDict.keys()
        try:
            outputs['nodeInstance'] = np.array([key[0].decode('UTF-8') for key in nodeKeys])
        except AttributeError:
            outputs['nodeInstance'] = np.array([key[0] for key in nodeKeys])
        outputs['nodeLabel'] = np.array([key[1] for key in nodeKeys])

    if len(reader.fieldSteps[stepname].frames[0].elementOutputs) > 0:

        eleOutput = reader.fieldSteps[stepname].elementData[0]
        eleKeys = reader.fieldSteps[stepname].frames[0].elementOutputs[eleOutput].dataDict.keys()
        # Try decoding the element keys from UTF-8. If it doesn't work, just use the eleKeys as-is.
        try:
            outputs['eleInstance'] = np.array([key[0].decode('UTF-8') for key in eleKeys])
        except AttributeError:  # In case it's already a string
            outputs['eleInstance'] = np.array([key[0] for key in eleKeys])
        outputs['eleLabel'] = np.array([key[1] for key in eleKeys])

    try:
        outputs['time'] = np.array([frame.time for frame in reader.fieldSteps[stepname].frames])
    except AttributeError:
        outputs['freq'] = np.array([frame.freq for frame in reader.fieldSteps[stepname].frames])

    # Convert the list-based representations to numpy ndarrays
    for key in outputData:

        tempData = list()
        for i in range(nFrames):
            tempData.append(reader.fieldSteps[stepname].getFrameData(i, key))
        outputs[key] = np.array(tempData)

    if args.save is None:
        fullfilename = args.odbname.split(os.path.sep)[-1]
        filename = fullfilename.split('.')[0] + '_' + stepname + '.pkl'
    else:
        filename = args.save

    with open(filename, 'wb') as fid:
        pickle.dump(outputs, fid)

except Exception as err:
    logging.error(traceback.format_exc())
    pass