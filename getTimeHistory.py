#    ******************************************************
#    *****            COPYRIGHT  (C)  2018            *****
#    *****           BY ATA ENGINEERING, INC.         *****
#    *****             ALL RIGHTS RESERVED            *****
#    ******************************************************
#
#    Redistribution and use in source and binary forms, with or without modification, are permitted
#    provided that the following conditions are met:
#
#    1. Redistributions of source code must retain the above copyright notice, this list of
#    conditions and the following disclaimer.
#
#    2. Redistributions in binary form must reproduce the above copyright notice, this list of
#    conditions and the following disclaimer in the documentation and/or other materials
#    provided with the distribution.
#
#    3. Neither the name of the copyright holder nor the names of its contributors may be used
#    to endorse or promote products derived from this software without specific prior written
#    permission.
#
#    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
#    OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
#    THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
#    EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
#    GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
#    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
#    NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
#    OF THE POSSIBILITY OF SUCH DAMAGE.
#
# Joe Schoneman
# joe.schoneman@ata-e.com
# June 2018

"""
Reads time history data from a particular step of an Abaqus ODB file, and saves to a pickle file.

Usage:

    abaqus python getTimeHistory.py <odbname.odb> [-step <stepname>] [-save <filename>]

"""

from odbAccess import *
import code
import argparse
import pickle
import numpy as np


parser = argparse.ArgumentParser(description = 'Obtain influence coefficients from a modal ODB')

parser.add_argument('odb', type = str,
                    help = 'Required name of ODB to read.')
parser.add_argument('-step', default = None, type = str,
                    help = 'Step to read history output from. Default is final step.')
parser.add_argument('-save', default = None, type = str,
                    help = 'Optional filename to save to. Default is <odb>_<step>_hist.pkl')


args = parser.parse_args()

db = openOdb(args.odb, readOnly = True)

if args.step != None:
    stepKey = args.step
else:
    stepKey = db.steps.keys()[-1]

step = db.steps[stepKey]

histories = step.historyRegions
dataDict = dict()
dataDict['start_time'] = step.totalTime - step.timePeriod

for hkey in histories.keys():
    hist = histories[hkey]
    for dkey in hist.historyOutputs.keys():
        data = hist.historyOutputs[dkey]
        dataDict[hkey + '_' + dkey] = np.array(data.data)

db.close()

if args.save is None:
    filename = args.odb.split('.')[0] + '_' + stepKey + '_hist.pkl'
else:
    filename = args.save

with open(filename, 'wb') as fid:
    pickle.dump(dataDict, fid, protocol = 1)
    # np.save(fid, dataDict)