#    ******************************************************
#    *****            COPYRIGHT  (C)  2018            *****
#    *****           BY ATA ENGINEERING, INC.         *****
#    *****             ALL RIGHTS RESERVED            *****
#    ******************************************************
#
#    Redistribution and use in source and binary forms, with or without modification, are permitted
#    provided that the following conditions are met:
#
#    1. Redistributions of source code must retain the above copyright notice, this list of
#    conditions and the following disclaimer.
#
#    2. Redistributions in binary form must reproduce the above copyright notice, this list of
#    conditions and the following disclaimer in the documentation and/or other materials
#    provided with the distribution.
#
#    3. Neither the name of the copyright holder nor the names of its contributors may be used
#    to endorse or promote products derived from this software without specific prior written
#    permission.
#
#    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
#    OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
#    THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
#    EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
#    GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
#    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
#    NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
#    OF THE POSSIBILITY OF SUCH DAMAGE.
#
# Joe Schoneman
# joe.schoneman@ata-e.com
# June 2018

"""
Program using abaqusReader to read the model information from an ODB and write it out to a pickle
file. The focus is on getting nodes and elements; section/property information is not extracted.

Calling sequence is

abaqus python odb2model.py <odbfilename.odb>
-fieldOutputs <List of field outputs which are extracted>
[-step <Optional step name to read data from>]

which will write out the model information to "odbfilename_model.odb." Note that this model
is associated with an OdbReader object so, to load it, you must have "abaqusReader.py" on
your Python path. If -step is left empty, then no response data is read.

TODO: Pulling out only specific instances would be useful.
TODO: Implementing additional property definitions might be helpful. In fact, one might
endeavour to simply replicate the entire Abaqus Python object model in a non-Abaqus environment,
this could probably be done automatically by looping through dictionaries and keys. The data
requirements might get to be fairly large, though.
"""

from abaqusReader import OdbReader
import logging
import traceback
import argparse
import os
import pickle

parser = argparse.ArgumentParser(description = 'Retrieve data from an Abaqus ODB and write to .tec file')
parser.add_argument('odbname', help = 'Required name of ODB (with extension)')
parser.add_argument('-step', required = False, default = None, help = 'Optional step name')
parser.add_argument('-fieldOutputs', nargs = '+', help = 'List of field outputs requested',
                    required = False)

args = parser.parse_args()


# Wrap in try/catch for graceful exit on error
try:
    reader = OdbReader(args.odbname, verbose = True)
    reader.readModel()

    # Optionally read data
    if args.step is not None:
        print('Reading data for step %s' % args.step)
        if args.fieldOutputs is None:
            print('No field outputs specified; no data read')
        else:
            reader.readStep(args.fieldOutputs, stepKey = args.step)

    # Pickle the reader object
    fullfilename = args.odbname.split(os.path.sep)[-1]
    filename = fullfilename.split('.')[0] + '_model.pkl'
    reader.db = []
    with open(filename, 'wb') as fid:
        pickle.dump(reader, fid)


except Exception as err:
    logging.error(traceback.format_exc())
    pass