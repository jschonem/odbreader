# abaqusReader.py
#
# Joe Schoneman
# ATA Engineering
# 11/16/2016

# TODO: The data retrieval functions don't work correctly (e.g. step.getFrameData(...))

import sys
import logging
import traceback
import code
import numpy
import warnings

# print(sys.version_info)
if sys.version_info[0] == 2: # Python 2, assume running in Abaqus kernel
    from odbAccess import *
    from abaqusConstants import *

'''Supporting Classes for AbaqusReader: Model Items'''
class Node:

    def __init__(self, absIdx, relIdx, position):
        self.absIdx = absIdx
        self.relIdx = relIdx
        self.position = position

    def __str__(self):
        return 'Node %i [%i]' % (self.relIdx, self.absIdx)

    __repr__ = __str__

class Element:

    def __init__(self, absIdx, relIdx, connectivity, eleType):
        self.absIdx = absIdx
        self.relIdx = relIdx
        self.nodesRel = [node.relIdx for node in connectivity]
        self.nodesAbs = [node.absIdx for node in connectivity]
        self.nodes = connectivity
        self.eleType = eleType

    def __str__(self):
        return 'Element %i [%i]' % (self.absIdx, self.relIdx)

    __repr__ = __str__

class FEModel:

    def __init__(self, name = 'placeholder'):

        self.name = name
        self.instances = list()
        self.instanceNames = dict()
        self.nNodes = 0
        self.nEles = 0

    def addInstance(self, instance):

        self.instances.append(instance)
        self.instanceNames[instance.name] = instance
        self.nNodes += len(instance.nodesAbs)
        self.nEles += len(instance.elesAbs)

    def __str__(self):
        return 'FEModel with %i instances' % len(self.instances)

    __repr__ = __str__


class Instance:

    def __init__(self, name):

        self.name = name
        self.nodesAbs = list()
        self.nodesRel = dict()
        self.elesAbs = list()
        self.elesRel = dict()

    def addNode(self, node):

        if ((len(self.nodesAbs)) != node.absIdx):
            raise Exception('Node abs idx %i (relative %i) supplied to position %i' % (node.absIdx,
                           node.relIdx, len(self.nodesAbs)))

        self.nodesAbs.append(node)
        self.nodesRel[node.relIdx] = node
        self.nNodes = len(self.nodesAbs)

    def addEle(self, ele):

        if ((len(self.elesAbs)) != ele.absIdx):
            raise Exception('Ele abs idx %i (relative %i) supplied to position %i' % (ele.absIdx,
                           ele.relIdx, len(self.elesAbs),))

        self.elesAbs.append(ele)
        self.elesRel[ele.relIdx] = ele
        self.nEles = len(self.elesAbs)

    def getNodesAbs(self, absIdx):
        nodelist = list()
        for idx in absIdx:
            nodelist.append(self.nodesAbs[idx])
        return nodelist

    def getNodesRel(self, relIdx):
        nodelist = list()
        for idx in relIdx:
            nodelist.append(self.nodesRel[idx])
        return nodelist

    def __str__(self):
        return 'Instance %s with %i nodes and %i elements' % (self.name, len(self.nodesAbs),
                                                              len(self.elesAbs))

    __repr__ = __str__


'''Supporting Classes for AbaqusReader: Data Items'''
class NodeDataList:

    def __init__(self, fieldLabel):

        self.fieldLabel = fieldLabel
        self.dataDict = dict()

    def addNode(self, label, rawData, instance):
        # Nodal data is stored as a float
        key = (instance, label)
        data = float(rawData)
        self.dataDict[key] = data

    def __getitem__(self, index):
        return self.dataList[index]

    def __call__(self, key):
        return self.dataDict[key]

    def __str__(self):
        return 'NodeDataList: %s; %i entries' % (str(self.fieldLabel),
                                              len(self.dataDict))

    __repr__ = __str__


class ElementDataList:

    def __init__(self, fieldLabel):

        self.fieldLabel = fieldLabel
        self.dataDict = dict()

    def addData(self, eleLabel, rawData, eleInstance):
        # This case is more complicated, since elements have a data entry for each integration point.
        # Element data is stored as a list.

        if (type(rawData) != numpy.ndarray):
            data = [float(rawData)]
        else:
            data = [float(s) for s in rawData]

        # Behavior depends on whether or not the given (instance, label) combination exists. Use
        # a dictionary for this purpose.
        key = (eleInstance, eleLabel)

        if self.dataDict.has_key(key):
            # Extend the existing entry
            self.dataDict[key].extend(data)
        else:
            # Otherwise, add a new entry
            self.dataDict[key] = data

    def __call__(self, instance, nodeLabel):
        key = (instance, nodeLabel)
        try:
            return self.dataDict[key]
        except KeyError:
            warnStr = 'Instance %s/Element %i does not contain field output %s; skipping' % (
                       instance, nodeLabel, self.fieldLabel)
            warnings.warn(warnStr)
            return None

    def __str__(self):
        return 'ElementDataList: %s; %i entries' % (str(self.fieldLabel),
                                              len(self.dataDict))

    __repr__ = __str__

class Frame:

    def __init__(self, time = None, freq = None):

        if (freq == None):
            self.time = time
        else:
            self.freq = freq

        self.nodeOutputs = dict()
        self.elementOutputs = dict()


    def addNodeDataList(self, nodeDataList):

        self.nodeOutputs[nodeDataList.fieldLabel] = nodeDataList

    def addElementDataList(self, eleDataList):

        self.elementOutputs[eleDataList.fieldLabel] = eleDataList

    def getNodeFieldLabels(self):
        return self.nodeOutputs.keys()

    def getElementFieldLabels(self):
        return self.elementOutputs.keys()

    def __getitem__(self, key):

        if (self.nodeOutputs.has_key(key)):
            return self.nodeOutputs[key]
        elif (self.elementOutputs.has_key(key)):
            return self.elementOutputs[key]
        else:
            raise KeyError

    def __str__(self):
        try:
            label = 'time'
            tf = self.time
        except AttributeError:
            label = 'freq'
            tf = self.freq
        return 'Frame: %s: %f; %i nodal data; %i element data; Nodal Outputs: %s; Element Outputs: %s' % (label, tf, len(self.nodeOutputs),
                                                                                                   len(self.elementOutputs),
                                                                                                   str(self.nodeOutputs.keys())[1:-1],
                                                                                                   str(self.elementOutputs.keys())[1:-1])
    __repr__ = __str__

class StepData:

    def __init__(self, name, procedure):

        self.nFrames = 0
        self.name = name
        self.procedure = procedure
        self.frames = list()
        self.nodeData = None
        self.elementData = None

    def addFrame(self, frame):
        '''TODO: Add checks that each frame has the same field outputs'''
        self.frames.append(frame)
        self.nFrames += 1
        self.nodeData = frame.nodeOutputs.keys()
        self.elementData = frame.elementOutputs.keys()

    def getFrameData(self, frameNumber, fieldLabel):
        ''' Get all data corresponding to field label for a given frame '''
        return list(self.frames[frameNumber][fieldLabel].dataDict.values())

    def getItemData(self, relIdx, fieldLabel):
        ''' Get all data from an item (node/element) and field label over all frames '''
        dataList = list()

        for frame in self.frames:
            dataList.append(frame[fieldLabel](relIdx))

    def __getitem__(self, index):
        # Use this to refer to frames of a step
        return self.frames[index]

    def __str__(self):

        return 'Step "%s", procedure %s, with %i frames; node data: %s; element data: %s' % (self.name, self.procedure, self.nFrames,
                                                                           str(self.nodeData)[1:-1],
                                                                           str(self.elementData)[1:-1])

    __repr__ = __str__

# Main class definition
class OdbReader:

    def __init__(self, odbfile, verbose = False):
        self.odbfile = odbfile

        # Open the ODB for reading
        self.db = openOdb(self.odbfile)
        self.odbdata = {'nodes': [], 'elements': [], 'field': dict(), 'history': dict()}
        self.verbose = verbose

        if self.verbose:
            print('Abaqus Output Database %s loaded' % self.odbfile)

        # Check precision
        if (self.db.jobData.precision == SINGLE_PRECISION):
            self.single = True
        else:
            self.single = False

        self.fem = FEModel()


    def readModel(self):
        # First make a separate pseudo-instance for the root assembly
        instance = self.db.rootAssembly
        inst = Instance('ASSEMBLY')

        absIdx = 0
        for node in instance.nodes:
            n = Node(absIdx, node.label, node.coordinates)
            inst.addNode(n)
            absIdx += 1

        # Get elements
        absIdx = 0
        for element in instance.elements:
            connectivity = inst.getNodesRel(element.connectivity)
            ele = Element(absIdx, element.label, connectivity, element.type)
            inst.addEle(ele)
            absIdx += 1

        self.fem.addInstance(inst)

        # Now go through each instance and get the nodes and elements
        instances = self.db.rootAssembly.instances

        for key in instances.keys():
            instance = instances[key]
            inst = Instance(key)

            #  Get nodes
            absIdx = 0
            for node in instance.nodes:
                n = Node(absIdx, node.label, node.coordinates)
                inst.addNode(n)
                absIdx += 1

            # Get elements
            absIdx = 0
            for element in instance.elements:
                connectivity = inst.getNodesRel(element.connectivity)
                ele = Element(absIdx, element.label, connectivity, element.type)
                inst.addEle(ele)
                absIdx += 1

            self.fem.addInstance(inst)


    def readStep(self, fieldnames, frameNumber = None, stepKey = None):
        # Read field data from selected step using selected fieldnames

        if stepKey is (None): # Default to last step in analysis
            stepKey = self.db.steps.keys()[-1]

        step = self.db.steps[stepKey]

        if (frameNumber == None): # Default to all frames in analysis
           frameList = step.frames
        else:
           frameList = [step.frames[frameNumber]]

        self.fieldSteps = {stepKey: StepData(stepKey, step.procedure)}

        if (self.verbose):
            print('Reading step "%s"; %i frames; domain = %s' % (stepKey, len(step.frames), step.domain))

        # Move through each frame
        k = 1
        for frame in frameList:
            if (self.verbose):
                print('\tFrame %i/%i' % (k, len(step.frames)))
                k += 1

            if (step.domain == TIME):
                time = frame.frameValue
                fr = Frame(time = time)
            elif ((step.domain == FREQUENCY) or (step.domain == MODAL)):
                freq = frame.frequency
                fr = Frame(freq = freq)

            for fieldname in fieldnames: # Now move through each output field
                try:
                    output = frame.fieldOutputs[fieldname]
                except KeyError:
                    print('WARNING: Field output %s not present in step %s' % (fieldname,
                                                                           step.name))
                    continue

                # Get data
                if (output.locations[0].position == NODAL):
                    componentDict, invariantDict = self.nodalOutput(output, fieldname)
                    for key in componentDict.keys():
                        fr.addNodeDataList(componentDict[key])
                    for key in invariantDict.keys():
                        fr.addNodeDataList(invariantDict[key])

                elif (output.locations[0].position == INTEGRATION_POINT):
                    componentDict, invariantDict = self.elementOutput(output, fieldname)
                    for key in componentDict.keys():
                        fr.addElementDataList(componentDict[key])
                    for key in invariantDict.keys():
                        fr.addElementDataList(invariantDict[key])

                elif (output.locations[0].position == WHOLE_ELEMENT):
                    componentDict, invariantDict = self.elementOutput(output, fieldname)
                    for key in componentDict.keys():
                        fr.addElementDataList(componentDict[key])
                    for key in invariantDict.keys():
                        fr.addElementDataList(invariantDict[key])

            # Append frame to step
            self.fieldSteps[stepKey].addFrame(fr)

        if self.verbose:
            print('Field data read complete')



    def nodalOutput(self, output, fieldLabel):
        # Given a field output, return a pair of NodeDataList dictionaries containing
        # ALL components and invariants available from the output
        # I need Abaqus to write this section properly.

        values = output.values
        # Get the components of this field output
        components = output.componentLabels
        componentDict = dict()
        for component in components:
            componentDict[component] = NodeDataList(component)
        # Stupid special case for scalar outputs that don't provide components
        if (len(components) == 0):
            components = [fieldLabel]
            componentDict[fieldLabel] = NodeDataList(fieldLabel)

        # Get the invariants of this field output
        invariants = ['magnitude']
        invariantDict = dict()
        value = values[0]
        for invariant in invariants:
            if (value.__getattribute__(invariant) == None):
                invariants.remove(invariant)
            else:
                invariantDict[invariant] = NodeDataList('%s%s' % (fieldLabel, invariant))

        # Now populate each NodeDataList from each value
        for value in values:
            # Note: If the value is associated with an assembly node, it will have an instance
            # value of "None" and as such does not have an instance name. So, for that special case,
            # assign the instance name as "None" and pass that through as the dictionary key to
            # be used.
            if value.instance is None:
                instanceName = None
            else:
                instanceName = value.instance.name
            relIdx = value.nodeLabel
            if (self.single):
                data = value.data
            else:
                data = value.dataDouble
            # Convert data from ndarrray or float to list
            try:
                data = list(data)
            except TypeError: # For scalar data
                data = [data]

            # Handle components
            for ii in range(len(components)):
                component = components[ii]
                datum = data[ii]
                componentDict[component].addNode(relIdx, datum, instanceName)

            # Handle invariants
            for invariant in invariants:
                datum = value.__getattribute__(invariant)
                invariantDict[invariant].addNode(relIdx, datum, instanceName)


        # Return the two dictionaries
        return componentDict, invariantDict

    def elementOutput(self, output, fieldLabel):
        # Given a field output, return a pair of ElementDataList dictionaries containing
        # ALL components and invariants available from the output

        values = output.values
        # Get the components of this field output
        components = output.componentLabels
        componentDict = dict()
        for component in components:
            componentDict[component] = ElementDataList(component)

        if (len(components) == 0):
            components = [fieldLabel]
            componentDict[fieldLabel] = ElementDataList(fieldLabel)

        # Get the key invariants of this field output
        invariants = ['mises', 'maxPrincipal']

        invariantDict = dict()
        value = values[0]
        for invariant in invariants:
            if (value.__getattribute__(invariant) != None):
                invariantDict[invariant] = ElementDataList('%s%s' % (fieldLabel, invariant))

        # Now populate each ElementDataList from each value
        k = 0
        for value in values:
            # Note: If the value is associated with an assembly element, it will have an instance
            # value of "None" and as such does not have an instance name. So, for that special case,
            # assign the instance name as "None" and pass that through as the dictionary key to
            # be used.
            if value.instance is None:
                instanceName = None
            else:
                instanceName = value.instance.name

            relIdx = value.elementLabel
            if (self.single):
                data = value.data
            else:
                data = value.dataDouble

            # Handle components
            if (type(data) != numpy.ndarray):
                data = [data]
            for ind, datum in enumerate(data):
                component = components[ind]
                componentDict[component].addData(relIdx, datum, instanceName)
            k += 1
#            if len(componentDict[component].dataList) != k:
#                code.interact(local = locals())
            # Handle invariants
            for invariant in invariantDict.keys():
                datum = value.__getattribute__(invariant)
                invariantDict[invariant].addData(relIdx, datum, instanceName)

        # Return the two dictionaries

        return componentDict, invariantDict


    def readHistory(self):
        '''Read ALL history data'''
        raise NotImplementedError


    # Functions to save and load data
    def save(self, filename):
        import pickle
        if self.verbose:
            print('Saving ODB data...')

        with open(filename + '.pkl', 'w') as fid:
            pickle.dump(self.odbdata, fid)

        if self.verbose:
            print('\tODB data saved to %s.pkl' % filename)


    def load(self, filename):
        import pickle
        if self.verbose:
            print('Loading ODB data...')
        with open(filename + '.pkl', 'r') as fid:
            self.odbdata = pickle.load(fid)

        if self.verbose:
            print('\tODB data loaded from %s.pkl' % filename)

    def close(self):
        # Close odb file before exiting
        self.db.close()


def printTest(reader):
    print(reader.fem)
    print(reader.fem.instances[0])
    print(reader.fem.instances[0].nodesAbs[0])
    print(reader.fem.instances[0].elesAbs[0])
    step = reader.fieldSteps[reader.fieldSteps.keys()[0]]
    print(step)
    print(step.frames[0])
    print(step.frames[0]['U1'][0])

if __name__ == '__main__':

    nargin = len(sys.argv)
    odbfile = sys.argv[1]


    # fieldOutputs = ['U', 'NT11', 'FV1', 'FV2', 'FV3', 'FV4']
    fieldOutputs = ['U', 'NT11']

    # Wrap in try/catch for graceful exit on error
    try:
        reader = OdbReader(odbfile, verbose = True)
        # reader.readModel()
        reader.readStep(fieldOutputs)
        printTest(reader)

    except Exception as err:
        logging.error(traceback.format_exc())
        pass

    reader.close()




