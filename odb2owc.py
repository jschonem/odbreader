#    ******************************************************
#    *****            COPYRIGHT  (C)  2018            *****
#    *****           BY ATA ENGINEERING, INC.         *****
#    *****             ALL RIGHTS RESERVED            *****
#    ******************************************************
#
#    Redistribution and use in source and binary forms, with or without modification, are permitted
#    provided that the following conditions are met:
#
#    1. Redistributions of source code must retain the above copyright notice, this list of
#    conditions and the following disclaimer.
#
#    2. Redistributions in binary form must reproduce the above copyright notice, this list of
#    conditions and the following disclaimer in the documentation and/or other materials
#    provided with the distribution.
#
#    3. Neither the name of the copyright holder nor the names of its contributors may be used
#    to endorse or promote products derived from this software without specific prior written
#    permission.
#
#    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
#    OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
#    THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
#    EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
#    GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
#    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
#    NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
#    OF THE POSSIBILITY OF SUCH DAMAGE.
#
# Joe Schoneman
# joe.schoneman@ata-e.com
# June 2018

'''
This is a specialized script which uses surface definitions in an Abaqus ODB to define a particular
surface file format for cosimulation.
'''

from odbAccess import *
import time
import argparse


parser = argparse.ArgumentParser(description = 'Use surface definition in an Abaqus ODB to define an "OWC" file for cosimulation.')
parser.add_argument('-odb', required = True, help = 'Required name of ODB.')
parser.add_argument('-surface', required = True, help = 'Name of surface to convert.')


args = parser.parse_args()

#METHOD DEFINITIONS
# node ordering defs: http://abaqus-docs:2080/v2016/books/usb/default.htm?startat=pt06ch28s01ael03.html#d0e313829

def TicTocGenerator():
    # Generator that returns time differences
    ti = 0           # initial time
    tf = time.time() # final time
    while True:
        ti = tf
        tf = time.time()
        yield tf-ti # returns the time difference


def toc(temp_bool = True):
    # Prints the time difference yielded by generator instance TicToc
    temp_time_interval = next(TicToc)
    if temp_bool:
        print( "Elapsed time: %f seconds.\n" %temp_time_interval )


def tic():
    # Records a time in TicToc, marks the beginning of a time interval
    toc(False)


def get_face_local_nodes(face, connectivity):
    # function to retrieve the nodes that compose an element surface
    # face is a string corresponding to the desired face
    # connectivity is a list of node IDs relative to a specific surface and instance
    num_nodes = len(connectivity)
    if num_nodes == 3:
        face_node_ref = {'SIDE1':[3,2,1], 'SIDE2':[1,2,3]}
    elif num_nodes == 4:
        face_node_ref = {'FACE1': [1, 2, 3], 'FACE2': [1, 4, 2], 'FACE3': [2, 4, 3], 'FACE4': [3, 4, 1], 'SIDE1':[4,3,2,1], 'SIDE2':[1,2,3,4]}
    elif num_nodes == 5:
        face_node_ref = {'FACE1': [1, 2, 3, 4], 'FACE2': [1, 5, 2], 'FACE3': [2, 5, 3], 'FACE4': [3, 5, 4],
                         'FACE5': [1, 5, 4]}
    elif num_nodes == 6:
        face_node_ref = {'FACE1': [1, 2, 3], 'FACE2': [4, 6, 5], 'FACE3': [1, 4, 5, 2], 'FACE4': [2, 5, 6, 3],
                         'FACE5': [3, 6, 4, 1]}
    elif num_nodes == 8:
        face_node_ref = {'FACE1': [1, 2, 3, 4], 'FACE2': [5, 8, 7, 6], 'FACE3': [1, 5, 6, 2], 'FACE4': [2, 6, 7, 3],
                         'FACE5': [3, 7, 8, 4], 'FACE6': [4, 8, 5, 1]}
    local_nodes = []
    for i in face_node_ref[face]:
        local_nodes.append(connectivity[i-1])
    return local_nodes


def get_global_nodes(name, local_nodes, global_node_ordering):
    # function to transform local nodes to global nodes
    # name is the name of a specific instance
    # local nodes is a list of nodeIDs local to an instance
    # globalNodeOrdering is a dictionary that maps an (instance name, localNodeID) tuple to a global node number
    global_nodes = []
    for i in local_nodes:
        global_nodes.append(global_node_ordering[(name, i)])
    return global_nodes

def split_face(nodes):
    split_scheme = ([1, 2, 4], [2, 3, 4])
    split_elements = []
    for scheme in split_scheme:
        split_nodes = [nodes[i-1] for i in scheme]
        split_elements.append([len(split_nodes), split_nodes])
    return split_elements

TicToc = TicTocGenerator()  # create an instance of the TicTocGen generator

# Open up ODB
db = openOdb(args.odb)

surface = db.rootAssembly.surfaces[args.surface]

tic()
# initialize dictionaries and lists
global_node_ordering = {}
node_coordinates = {}
final_element_list = []

# create a list that stores the instance name and node label
surface_nodes = surface.nodes

node_number = 1
for instance_nodes in surface_nodes:
    for node in instance_nodes:
        key = (node.instanceName, node.label)
        if key not in global_node_ordering:
            global_node_ordering[key] = node_number
            node_coordinates[node_number] = node.coordinates
            node_number += 1


if type(surface.elements) is not type(None):
    for a, item in enumerate(surface.elements):
        face_list = surface.faces[a]
        for b, element in enumerate(item):
            element_face = str(face_list[b])
            element_connectivity = element.connectivity
            instance_name = element.instanceName
            localNodes = get_face_local_nodes(element_face, element_connectivity)
            globalNodes = get_global_nodes(instance_name, localNodes, global_node_ordering)
            if len(globalNodes) == 4:
                split_elements = split_face(globalNodes)
                final_element_list.extend(split_elements)
            else:
                final_element_list.append([len(globalNodes), globalNodes])


    #PRINTING DATA TO .DAT FILE
    output_filename = 'owc.dat'
    with open(output_filename, 'w') as f:

        node_info = '# of surfaces:\n%d\n' \
                    '# number of solutions:\n%d\n' \
                    '# number of node variables:\n%d\n' \
                    '# node variable names:\n%d\n' \
                    '# number of element variables:\n%d\n' \
                    '# element variable names:\n%s\n' \
                    '# number of nodes:\n%d\n'\
                    '# Node positions:\n'%(1, 0, 0, 0, 0, 'none', len(global_node_ordering))

        f.write(node_info)

        for node in node_coordinates:
            coordinates = (node_coordinates[node])
            s = '{:12.11f} {:12.11f} {:12.11f}\n'.format(coordinates[0], coordinates[1], coordinates[2])
            f.write(s)

        element_info = '# Number of faces:\n%d\n' \
                       '# Surface face definition (#pnts nodes)):\n' %(len(final_element_list))

        f.write(element_info)

        for element in final_element_list:
            num_nodes = str(element[0])
            f.write(num_nodes)
            global_node_ids = element[1]
            for node in global_node_ids:
                s = ' {:d}'.format(node)
                f.write(s)
            f.write('\n')

    print("Extraction Complete. Results File: %s\n" % output_filename)
    toc()
