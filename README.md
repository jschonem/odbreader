# README #

ODBReader is a set of Python modules and scripts for reading Abaqus FEA results from output database (ODB) files and
writing to external formats -- Python Pickle or MATLAB .mat files.

### What do I need to run? ###

To run the ODB extraction files, you must have a valid Abaqus installation; this will include a bare-bones version of
Python 2 (usually 2.6 or 2.7). The postprocessing example uses Numpy and MatPlotLib, so you must have Python installed on
your own system, along with the requisite libraries, to run these. 

### How do I get set up? ###

Clone the repository or download the files and place them where desired. For example use cases, see the "example" folder.

### Who do I talk to? ###

This was written and is informally maintained by Joe Schoneman. I can be reached at joe.schoneman@ata-e.com or jdschoneman@gmail.com

### License ###

ODBReader is released under the 3-clause BSD license. The copyright, license, and indemnification statement are reproduced below.


******************************************************
            COPYRIGHT  (C)  2018            
           BY ATA ENGINEERING, INC.         
             ALL RIGHTS RESERVED            

 
Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

******************************************************

