# -*- coding: utf-8 -*-
"""
Write AbaqusReader data to TEC files. 

"""

import numpy as np
import code

class TecWriter:
    
    def __init__(self, abReader, verbose = False):
        
        self.abReader = abReader
        self.stepKeys = abReader.fieldSteps.keys()
        self.verbose = verbose

    def writeTec(self, tecfile, elementType, stepKey = None):
        '''Write specified step to a .tec file'''
        if (stepKey == None):
            stepKey = self.stepKeys[0]

        if self.verbose:
            print('Writing step %s data to %s...' % (stepKey, tecfile))
            
        with open(tecfile, 'w') as self.fid:
            self.fid.write('TITLE = %s\n' % self.abReader.fem.name)
            
            # Create list of variables
            varList = 'VARIABLES = "X", "Y", "Z"'
            
            # Add any nodal and elemental outputs to the output list
            step = self.abReader.fieldSteps[stepKey]
            nodeOutputList = step[0].nodeOutputs.keys()
            eleOutputList = step[0].elementOutputs.keys()

            for output in nodeOutputList:
                varList += ', "%s"' % output
                    
            for output in eleOutputList:
                varList += ', "%s"' % output
                
            self.fid.write(varList + '\n')
            
            # Also take care of the "VARLOCATION" call
            if len(eleOutputList) == 0:
                varlocLine = ''
            else:
                start = len(nodeOutputList) + 4
                centeredVars = range(start, start + len(eleOutputList))
                varlocLine = 'VARLOCATION=(%s=CELLCENTERED), ' % (str(centeredVars))
                
            # Get node and element counts
            nNodes = self.abReader.fem.nNodes
            nEles = self.abReader.fem.nEles
            
            # Now write each frame
            first = True
                
            for frame in step.frames: 
                title = '%s-FRAME_%i' % (step.name, step.frames.index(frame))
                time = frame.time
                if first:
                    connectivityString = ''
                else:
                    connectivityString = ', CONNECTIVITYSHAREZONE = 1' # StrandID=1,
                self.fid.write('ZONE T="%s", SOLUTIONTIME=%.6f, DATAPACKING=BLOCK, NODES=%i, ELEMENTS=%i, %sZONETYPE=%s%s\n' % (title, time,
                                                                                                                                nNodes,
                                                                                                                                nEles,
                                                                                                                                varlocLine,
                                                                                                                                elementType,
                                                                                                                                connectivityString))
                                                                                                  
                # Write actual location, assuming that U is available. Will fail otherwise.
                #print('CODE.INTERACT PLACED AT LINE 76 OF TECWRITER.PY')
                #code.interact(local = locals())
                self.writeLocation(self.abReader.fem, frame)
                
                # Write remaining nodal data
                self.writeNodeData(self.abReader.fem, frame, nodeOutputList)
                
                # Write elemental data
                self.writeElementData(self.abReader.fem, frame, eleOutputList)
                     
                # Write element connectivity if this is the first frame
                if first:
                    if elementType == 'FEQUADRILATERAL':
                        minNodes = 4
                    elif elementType == 'FBRICK':
                        minNodes = 8
                    else:
                        raise ValueError('Element type not recognized')
                    self.writeElementConnectivity(self.abReader.fem, minNodes)
                    first = False
                
            if self.verbose:
                print('Write Complete')
    
    def writeLocation(self, fem, frame):
        '''Write either the adjusted location data (if U or COORD is available) 
        or the base coordinates for the first 3 lines.'''
        # Case: 'U1' present in the output list
        if (frame.nodeOutputs.has_key('U1')):
            keys = ['U1', 'U2', 'U3']
            if (not frame.nodeOutputs.has_key('U3')):
                keys.pop('U3') # In case model is 2D
            for key in keys:
                index = keys.index(key)
                outList = list()
                for node in fem.instances[0].nodesAbs: # Only does a single instance right now
                    coord = node.position[index] + frame[key](node.relIdx)
                    outList.append(coord)
                self.writeList(outList)
                
        elif (frame.nodeOutputs.has_key('COORD1')):
            keys = ['COORD1', 'COORD2', 'COORD3']
            if (not frame.nodeOutputs.has_key('COORD3')):
                keys.pop('COORD3') # In case model is 2D
            for key in keys:
                index = keys.index(key)
                outList = list()
                for node in fem.instances[0].nodesAbs:
                    outList.append(frame[key](node.relIdx))
                self.writeList(outList)       
        else:
            for ii in range(3):
                outList = list()
                for node in fem.instances[0].nodesAbs:
                    outList.append(node.position[ii])
                self.writeList(outList)   
            
    def writeNodeData(self, fem, frame, nodeOutputList):
        
        for fieldLabel in nodeOutputList:
            outList = list()
            for node in fem.instances[0].nodesAbs:
                # Add nodal value to outlist
                outList.append(frame[fieldLabel](node.relIdx))
                
            self.writeList(outList)
            
    
    def writeElementData(self, fem, frame, eleOutputList):   
        
        for fieldLabel in eleOutputList:
            outList = list()
            for element in fem.instances[0].elesAbs:
                # Average the integration values
                
                intPointValues = frame[fieldLabel](element.relIdx)
                if intPointValues == None:
                    code.interact(local=locals()) # Debug line
                    
                outList.append(np.mean(intPointValues))

            self.writeList(outList)
        
    def writeElementConnectivity(self, fem, minNodes):
        for element in fem.instances[0].elesAbs:
            outList = [s + 1 for s in element.nodesAbs] # Nodes are indexed from 0 in abReader
            
            # Some workarounds for unexpected element types
            if (len(outList) < minNodes): # uh oh!
               for i in range(minNodes - len(outList)):
                   outList.append(outList[i])
            self.fid.write(str(outList)[1:-1].replace(',', '') + '\n')
        
    # Print an array to file as a space-delimited list. Appends newline character
    def writeList(self, line):
        limit = 8 # Number of entries per line to enter
        N = len(line)
        m = (N - (N % limit))/limit
        newLines = list()
        for i in range(m):
            newLines.append(line[(i*limit):((i + 1)*limit)])
        newLines.append(line[((i + 1)*limit):])
        
        for newLine in newLines:
            outStr = ''
            for item in newLine:
                outStr += '%.8E ' % item
            # outStr = str(newLine)[1:-1].replace(',', '')
            self.fid.write(outStr + '\n')