# -*- coding: utf-8 -*-
"""
Program using abaqusReader and tecWriter modules to read a step and write
it out to TEC format. 

Calling sequence is

abaqus python odb2tec.py odbfilename.odb [stepname] [tecfilename]

with bracketd arguments optional. Default step is last step in ODB; default
.tec filename is 'odbfilename.tec' written to the calling directory. 

"""
from tecWriter import TecWriter
from abaqusReader import OdbReader
import logging
import traceback
import argparse

import pickle

parser = argparse.ArgumentParser(description = 'Retrieve data from an Abaqus ODB and write to .tec file')
parser.add_argument('odbname', help = 'Required name of ODB (with extension)')
parser.add_argument('-step', default = None, help = 'Optional step name')
parser.add_argument('-fieldOutputs', nargs = '+', help = 'List of field outputs requested', 
                    required = True)
parser.add_argument('-elementType', required = True, help = 'TECPlot Element Type: FEBRICK | FEQUADRILATERAL')

args = parser.parse_args()

 
# Wrap in try/catch for graceful exit on error
try:
    reader = OdbReader(args.odbname, verbose = True)
    reader.readModel()
    reader.readStep(args.fieldOutputs, stepKey = args.step)
    
    # Pickle the reader to use later
    filename = args.odbname.split('.')[0] + '.pkl'
    reader.db = []
    with open(filename, 'wb') as fid:
        pickle.dump(reader, fid, protocol = 2)
    
    
    writer = TecWriter(reader, verbose = True)
    tecfile = args.odbname.split('.')[0] + '.tec'
    writer.writeTec(tecfile, args.elementType)
    
except Exception as err:
    logging.error(traceback.format_exc())
    pass