#    ******************************************************
#    *****            COPYRIGHT  (C)  2018            *****
#    *****           BY ATA ENGINEERING, INC.         *****
#    *****             ALL RIGHTS RESERVED            *****
#    ******************************************************
#
#    Redistribution and use in source and binary forms, with or without modification, are permitted
#    provided that the following conditions are met:
#
#    1. Redistributions of source code must retain the above copyright notice, this list of
#    conditions and the following disclaimer.
#
#    2. Redistributions in binary form must reproduce the above copyright notice, this list of
#    conditions and the following disclaimer in the documentation and/or other materials
#    provided with the distribution.
#
#    3. Neither the name of the copyright holder nor the names of its contributors may be used
#    to endorse or promote products derived from this software without specific prior written
#    permission.
#
#    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
#    OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
#    THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
#    EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
#    GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
#    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
#    NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
#    OF THE POSSIBILITY OF SUCH DAMAGE.
#
# Joe Schoneman
# joe.schoneman@ata-e.com
# June 2018

"""
This script loads a pickle file and saves it as a MATLAB .mat file. Any dots that are present
in the variable keys will be replaced with underscores in order to comply with MATLAB's variable
naming rules

USAGE:

python pickle2mat.py <nameOfPickleFile.extension>

Will create the file <nameOfPickleFile.mat> in the current directory.

"""

import pickle
import scipy.io
import sys

filename = sys.argv[1]

with open(filename, 'rb') as fid:
    data = pickle.load(fid, encoding = 'latin1')
matname = filename.split('.')[0]

# Remove any dots from variable keys
newData = dict()
for key, value in data.items():
    key = key.replace('.', '_').replace('-', '_').replace(' ', '_')
    newData[key] = value
scipy.io.savemat('%s.mat' % matname, newData)

