# -*- coding: utf-8 -*-
"""

Demo script for ODBReader. Loads field and history results from an ODB and makes a few plots
to show how to access the ODB data.

"""

import pickle
import numpy as np
import matplotlib.pyplot as plt
import os
import sys

save = True  # Flag for saving images

# Load in the data
readmode = 'rb'
with open('analysis_static.pkl', readmode) as fid:
    # Have to use the latin1 encoding when reading Pickle files written out from within the Abaqus
    # kernel due to its older version of Python.
    static = pickle.load(fid, encoding = 'latin1')

with open('analysis_modal.pkl', readmode) as fid:
    modal = pickle.load(fid, encoding = 'latin1')

with open('analysis_static_hist.pkl', readmode) as fid:
    hist = pickle.load(fid, encoding = 'latin1')

# Load in the model itself. Manually add in the AbaqusReader path since we usually don't
# have it actually installed on the Python Path. This assumes that AbaqusReader is located on the
# directory above the working directory.
abReaderPath = os.path.abspath('..')
if abReaderPath not in sys.path:
    sys.path.append(abReaderPath)

with open('analysis_model.pkl', readmode) as fid:
    model = pickle.load(fid, encoding = 'latin1')
    fem = model.fem

# Plot 1: Pseudo-time history of the midpoint displacement. Show the history output with all
# points along with the field output with just a few points
# History output for node 127 (requested as a specific output)
key = 'Node PANEL.127_U3'
data = hist[key]
t = data[:, 0]
y = data[:, 1]

# Field output for node 127 (pulled from field outputs)
nodeLabels = static['nodeLabel']
nodeIndex = np.where(nodeLabels == 127)[0][0]
U3 = static['U3'] # Output data, with each row corresponding to a single output frame
tField = static['time'] # Field output time vector
yField = U3[:, nodeIndex]

fig = plt.figure(figsize = (12, 6))
plt.plot(t, y, label = 'History Output')
plt.plot(tField, yField, 'ko', label = 'Field Output')
plt.xlabel('Pseudo-Time', fontsize = 14, fontweight = 'bold')
plt.ylabel('Displacement [m]', fontsize = 14, fontweight = 'bold')
plt.xlim([-0.025, 1.025])
if save: fig.savefig('plot1.png', transparent = True, bbox_inches = 'tight')

# Plot 2: Side view of displaced shape of the structure.
# Get node locations from the FEM object.
panel = fem.instances[1]
nodeX = [panel.nodesRel[s].position[0] for s in nodeLabels]
nFrame = U3.shape[0]

fig = plt.figure(figsize = (12, 6))
for i in range(1, nFrame):
    plt.plot(nodeX, U3[i, :], '.', label = 'Frame %i' % i)
plt.xlabel('Position [m]', fontsize = 14, fontweight = 'bold')
plt.ylabel('Displacement [m]', fontsize = 14, fontweight = 'bold')
plt.legend()
if save: fig.savefig('plot2.png', transparent = True, bbox_inches = 'tight')

# Plot 3: First 3 modes and frequencies of the statically loaded structure.
U3m = modal['U3']
freqs = modal['freq']
nModes = U3m.shape[0]
fig, ax = plt.subplots(1, 3)
fig.set_figheight(3)
fig.set_figwidth(14)

for i in range(1, 4):
    ax[i - 1].plot(nodeX, U3m[i, :], '.')
    ax[i - 1].set_title('Mode %i; %.1f Hz' % (i, freqs[i]))

ax[1].set_xlabel('Position [m]', fontsize = 14, fontweight = 'bold')
ax[0].set_ylabel('Modal Displacement', fontsize = 14, fontweight = 'bold')
if save: fig.savefig('plot3.png', transparent = True, bbox_inches = 'tight')