This example shows how to perform some of the basic read operations on an Abaqus ODB. It should contain the following files:

- model.inp: An Abaqus model input file which contains a model of a curved panel.
- analysis.inp: An Abaqus input file which will perform a static and modal analysis of the structure in "model.inp"
- plotResults.py: A Python file that will load results from the generated Pickle file and make a couple of plots.
- demo.bat: A batch script (Windows) which will run the panel.inp job, use odb2pkl.py to extract the results, and 
  run plotResults.py to show to access and use data obtained from the ODB. This will create 3 images in the working
  directory.

Make sure to run "demo.bat" from this "example" directory, because it will use relative paths in order to locate the
Python files which it is using.
