set ABJOB=analysis
set PYFIELD=../odb2pkl.py
set PYHIST=../getTimeHistory.py
set PYMODEL=../odb2model.py
set PYPLOT=plotResults.py

REM Run the analysis
call abaqus j=%ABJOB% inter ask_delete=off

REM Extract response field data from ODB
call abaqus python %PYFIELD% %ABJOB%.odb -fieldOutputs U -step static
call abaqus python %PYFIELD% %ABJOB%.odb -fieldOutputs U -step modal

REM Extract history data from ODB
call abaqus python %PYHIST% %ABJOB%.odb -step static

REM Extract the model to get node locations/element connectivity
call abaqus python %PYMODEL% %ABJOB%.odb

REM Plot the results and save images
python plotResults.py

pause